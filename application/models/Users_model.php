<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {
    public function __construct(){
    	  //Para indica classe nova User_model e não a herdada.
    	parent::__construct();
    	$this->load->database();
    }

    public function get_user_data($user_login){

    	$this->db
    		->select("user_id, password, user_full_name, email")
    		->from("users")
    		->where("user_login", $user_login); //seleciona a tabela e requisitos quando for o parâmetro.

    	$result = $this->db->get(); //Pega todos os resultados trazidos do banco.
    	
    	if ($result->num_rows() > 0) { //Retorna quantidade tuplas se for maior ou não.
    		 		return $result->row(); //retorna a quantidades completa
    		 	} else {
    		 		return NULL;
    		 	}
    		 		 	
    }

}