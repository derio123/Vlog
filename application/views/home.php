<!-- Header -->
<header>
	<div class="container">
		<div class="slider-container">
			<div class="intro-text">
				<div class="intro-lead-in">Bem vindo ao Vlog</div>
				<div class="intro-heading">Veja os meus novos projetos</div>
				<a href="#about" class="page-scroll btn btn-xl">Veja mais sobre mim</a>
			</div>
		</div>
	</div>
</header>
<section id="about" class="light-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="section-title">
					<h2>Sobre Mim</h2>
					<p>Nesta seção você saberá mais sobre mim e da minha profissão</p>
						<div class="alert alert-warning">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Observação!</strong> Estas e outras informações abaixo abaixo estarão disponíveis 
							na seção de portfólio que falará onde está meus projetos.
						</div>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- about module -->
			<div class="col-md-3 text-center">
				<div class="mz-module-about">
					<img src="<?php echo base_url(); ?>public/images/webdevelopment.png">
					<!-- <i class="fa fa-briefcase ot-circle"></i> -->
					<h3> Desenvolvimento Web</h3>
					<p>Nesta parte mostará as experiências e projetos em diversas tecnologias
					para sistemas web.</p>
				</div>
			</div>
			<!-- end about module -->
			<!-- about module -->
			<div class="col-md-3 text-center">
				<div class="mz-module-about">
					<img src="<?php echo base_url(); ?>public/images/cv.png">
					<!-- <i class="fa fa-photo ot-circle"></i> -->
					<h3>Visualizar Currículo</h3>
					<p>Aqui você saberá meus conhecimentos e minhas 
					experiências na área de Desenvolvimento de sistemas.</p>
				</div>
			</div>
			<!-- end about module -->
			<!-- about module -->
			<div class="col-md-3 text-center">
				<div class="mz-module-about">
					<img  src="<?php echo base_url(); ?>public/images/mobiledevelopment.png">
					<h3>Desenvolvimento Mobile</h3>
					<p></p>
				</div>
			</div>
			<!-- end about module -->
			<!-- about module -->
			<div class="col-md-3 text-center">
				<div class="mz-module-about">
					<img style="width:50%"  src="<?php echo base_url(); ?>public/images/Derio77.jpg">
					<h3>Sobre Mim</h3>
					<p> Itaque earum rerum hic tenetur a sapiente, ut aut reiciendis maiores</p>
				</div>
			</div>
			<!-- end about module -->
		</div>
	</div>
	<!-- /.container -->
</section>
<section class="overlay-dark bg-img1 dark-bg short-section">
	<div class="container text-center">
		<div class="row">
			<div class="col-md-3 mb-sm-30">
				<div class="counter-item">
					<h6>Anúncio 1</h6>
					<!-- <h2 data-count="59">59</h2> -->
					
				</div>
			</div>
			<div class="col-md-3 mb-sm-30">
				<div class="counter-item">
					
					<h6>Anúncio 2</h6>
				</div>
			</div>
			<div class="col-md-3 mb-sm-30">
				<div class="counter-item">
					<h6>Anúncio 3</h6>
				</div>
			</div>
			<div class="col-md-3 mb-sm-30">
				<div class="counter-item">
					<h6>Anúncio 4</h6>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="portfolio" class="light-bg">
	<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<div class="section-title">
				<h2>Portfólio de Projetos pessoais</h2>
				<p>Categorias.</p>
			</div>
		</div>
	</div>
	<div class="row">
		<!-- start portfolio item -->
		<div class="col-md-4">
			<div class="ot-portfolio-item">
				<figure class="effect-bubba">
					<img src="<?php echo base_url(); ?>public/images/40web-development_102111.png" alt="img02" class="img-responsive" />
					<figcaption>
						<h2>Desenvolvimento Web</h2>
						<p>CakePhp, Laravel, Ajax, ReactJs, Angular, Boostrap...</p>
						<a href="#" data-toggle="modal" data-target="#Modal-1">View more</a>
					</figcaption>
				</figure>
			</div>
		</div>
		<!-- end portfolio item -->
		<!-- start portfolio item -->
		<div class="col-md-4">
			<div class="ot-portfolio-item">
				<figure class="effect-bubba">
					<img src="<?php echo base_url(); ?>public/images/Android.png" alt="img02" class="img-responsive" />
					<figcaption>
						<h2>Desenvolvimento Mobile</h2>
						<p>Ionic, Android Studio & React</p>
						<a href="#" data-toggle="modal" data-target="#Modal-2">View more</a>
					</figcaption>
				</figure>
			</div>
		</div>
		<!-- end portfolio item -->
		<!-- start portfolio item -->
		<div class="col-md-4">
			<div class="ot-portfolio-item">
				<figure class="effect-bubba">
					<img src="<?php echo base_url(); ?>public/images/github.png" alt="img02" class="img-responsive" />
					<figcaption>
						<h2>Repositórios</h2>
						<p>Gihub, bitbucket, Gitlab</p>
						<a href="#" data-toggle="modal" data-target="#Modal-3">View more</a>
					</figcaption>
				</figure>
			</div>
		</div>
		<!-- end portfolio item -->
	</div>
	<div class="section-title text-center">
		<h2>Portfólio de Projetos de campo</h2>
	</div>
	<div class="row">
		<!-- start portfolio item -->
		<div class="col-md-4">
			<div class="ot-portfolio-item">
				<figure class="effect-bubba">
					<img src="<?php echo base_url(); ?>public/images/APFinanças.png" alt="img02" class="img-responsive" />
					<figcaption>
						<h2>Projetos APFinanças com Ionic 3</h2>
						<p>Criado no Projeto intregador 2 no 5°semestre de sistemas de informação</p>
						<p>APFinanças<></p>
						<a href="#" data-toggle="modal" data-target="#Modal-4">View more</a>
					</figcaption>
				</figure>
			</div>
		</div>
		<!-- end portfolio item -->
		<!-- start portfolio item -->
		<div class="col-md-4">
			<div class="ot-portfolio-item">
				<figure class="effect-bubba">
					<img src="<?php echo base_url(); ?>public/images/java.png" alt="img02" class="img-responsive" />
					<figcaption>
						<h2>Show do Milhão </h2>
						<p>Criado no 4°semestre de sistemas de informação</p>
						<p>Java</p>
						<a href="#" data-toggle="modal" data-target="#Modal-5">View more</a>
					</figcaption>
				</figure>
			</div>
		</div>
		<!-- end portfolio item -->
		<!-- start portfolio item -->
		<div class="col-md-4">
			<div class="ot-portfolio-item">
				<figure class="effect-bubba">
					<img src="<?php echo base_url(); ?>public/images/codeang1.jpg" alt="img02" class="img-responsive" />
					<figcaption>
						<h2>Em breve Sistema de patrimônio</h2>
						<p>Angular & Codelgniter</p>
						<a href="#" data-toggle="modal" data-target="#Modal-2">View more</a>
					</figcaption>
				</figure>
			</div>
		</div>
		<!-- end portfolio item -->
	</div>
	</div><!-- end container -->
</section>

<section id="team" class="light-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="section-title">
					<h2>Our Team</h2>
					<p>A creative agency based on Candy Land, ready to boost your business with some beautifull templates. Lattes Agency is one of the best in town see more you will be amazed.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- team member item -->
			<div class="col-md-3">
				<div class="team-item">
					<div class="team-image">
						<img src="<?php echo base_url(); ?>public/images/demo/author-2.jpg" class="img-responsive" alt="author">
					</div>
					<div class="team-text">
						<h3>TOM BEKERS</h3>
						<div class="team-location">Sydney, Australia</div>
						<div class="team-position">– CEO & Web Design –</div>
						<p>Mida sit una namet, cons uectetur adipiscing adon elit. Aliquam vitae barasa droma.</p>
					</div>
				</div>
			</div>
			<!-- end team member item -->
			<!-- team member item -->
			<div class="col-md-3">
				<div class="team-item">
					<div class="team-image">
						<img src="<?php echo base_url(); ?>public/images/demo/author-6.jpg" class="img-responsive" alt="author">
					</div>
					<div class="team-text">
						<h3>LINA GOSATA</h3>
						<div class="team-location">Los Angeles, California</div>
						<div class="team-position">– Photography –</div>
						<p>Worsa dona namet, cons uectetur dipiscing adon elit. Aliquam vitae fringilla unda mir.</p>
					</div>
				</div>
			</div>
			<!-- end team member item -->
			<!-- team member item -->
			<div class="col-md-3">
				<div class="team-item">
					<div class="team-image">
						<img src="<?php echo base_url(); ?>public/images/demo/author-3.jpg" class="img-responsive" alt="author">
					</div>
					<div class="team-text">
						<h3>Larry Parker</h3>
						<div class="team-location">Barcelona, Spain</div>
						<div class="team-position">– Development –</div>
						<p>Aradan bes namet, cons uectetur moiscing adon elit. Aliquam vitae fringilla unda augue.</p>
					</div>
				</div>
			</div>
			<!-- end team member item -->
			<!-- team member item -->
			<div class="col-md-3">
				<div class="team-item">
					<div class="team-image">
						<img src="<?php echo base_url(); ?>public/images/demo/author-4.jpg" class="img-responsive" alt="author">
					</div>
					<div class="team-text">
						<h3>John BEKERS</h3>
						<div class="team-location">Miami, Floria</div>
						<div class="team-position">– Marketing –</div>
						<p>Dolor sit don namet, cons uectetur beriscing adon elit. Aliquam vitae fringilla unda.</p>
					</div>
				</div>
			</div>
			<!-- end team member item -->
		</div>
	</div>
</section>
<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="section-title">
					<h2>Contato</h2>
					<p>Se você tem dúvidas ou precisa de ajuda! Entre em contato!</p>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- <div class="col-md-3">
				<h3>Our Business Office</h3>
				<p>3422 Street, Barcelona 432, Spain, New Building North, 15th Floor</p>
				<p><i class="fa fa-phone"></i> +101 377 655 22125</p>
				<p><i class="fa fa-envelope"></i>mail@yourcompany.com</p>
			</div>
			<div class="col-md-3">
				<h3>Business Hours</h3>
				<p><i class="fa fa-clock-o"></i> <span class="day">Weekdays:</span><span>9am to 8pm</span></p>
				<p><i class="fa fa-clock-o"></i> <span class="day">Saturday:</span><span>9am to 2pm</span></p>
				<p><i class="fa fa-clock-o"></i> <span class="day">Sunday:</span><span>Closed</span></p>
			</div> -->
			<div class="col-md-12">
				<form name="sentMessage" id="contactForm" novalidate="">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Seu Nome *" id="name" required="" data-validation-required-message="Por favor entre com seu nome.">
								<p class="help-block text-danger"></p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<input type="email" class="form-control" placeholder="Seu Email *" id="email" required="" data-validation-required-message="Por favor entre com endereço email.">
								<p class="help-block text-danger"></p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<textarea class="form-control" placeholder="Sua messangem *" id="message" required="" data-validation-required-message="Insira sua message."></textarea>
								<p class="help-block text-danger"></p>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-lg-12 text-center">
							<div id="success"></div>
							<button type="submit" class="btn">Enviar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<p id="back-top">
	<a href="#top"><i class="fa fa-angle-up"></i></a>
</p>